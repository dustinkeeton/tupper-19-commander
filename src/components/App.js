import React, { Component } from 'react';
import Aux from '../hoc/Aux';

import Game from '../containers/game';
import GameOver from '../containers/game_over'

class App extends Component {

    render() {
        return (
            <Aux>
                <Game/>
            </Aux>
        );
    }
}

export default App;