const SerialPort = require( "electron" ).remote.require( "serialport" );
import {Actions} from '../constants';

const serialPortMiddleWare = () => {

    return store => {
        const {
            dispatch
        } = store;
        SerialPort.list((err, ports) => {
            ports.forEach((port) => {

                if(port.comName.startsWith('/dev/tty.usbmodem')) {

                    console.log(port.comName);

                    var serialPort = new SerialPort(port.comName, {
                        baudRate: 9600,
                        parser: new SerialPort.parsers.ByteLength({length: 1})
                    });

                    serialPort.on("open", function () {
                        serialPort.on('data', function (data) {
                            console.log(data);
                        });
                    });

                    let buffer = new Buffer(1);
                    buffer[0] = 0x0;
                    serialPort.write(buffer);
                }
                else
                {
                    console.log(port.comName);
                }
            });
        });


        // var serialPort = new SerialPort("/dev/cu.usbmodem14601", {
        //     baudRate: 9600,
        //     parser: new SerialPort.parsers.ByteLength({length: 1})
        // });
        //
        // serialPort.on("open", function () {
        //
        //     serialPort.on('data', function(data) {
        //         if(data[0] == 0xFF)
        //         {
        //             dispatch({type: Actions.step_win});
        //         }
        //     });
        // });

        return next => action => {
            if (action.type === Actions.try_pattern) {
                const {
                    console, pattern
                } = action.data;
                var buffer = new Buffer(1);
                buffer[0] = 0x13;
                serialPort.write(buffer);
            }
            return next(action);
        };
    };


};

export default serialPortMiddleWare;
