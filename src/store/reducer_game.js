import { ipcRenderer } from 'electron';
import {GameState, ConsoleState, Actions} from '../constants';

const initialState = {
    game_state: GameState.idle,
    console_states: [ConsoleState.idle, ConsoleState.idle, ConsoleState.idle],
    fuel: 10,
    pressure: 1,
    temperature: 1,
    time_left: 15,
    drill_depth: 0,
    temperature_max: 10,
    pressure_max: 10,
    step_time: 15,
    fuel_min: 0,
    cheese_depth: 5,
    step_index: 0,
    step() {
        return steps[this.step_index];
    },
};

export const steps = [
    // {
    //     prompt: [
    //         'Problem: Increase ze drill depth!',
    //         'Engineer: Please move the secondary drill manifold to position 2 and engage the cut-out baffler.',
    //         'Foreman: Set the cheese level to 7 and shift the drill clutch to a number between 2 and 4.',
    //         "Morale Officer: Yell into the microphone to encourage your minions... err... co-workers."
    //     ],
    //     win: (state) => {
    //         const newFuel = state.fuel - 1;
    //         const newDrill_depth = state.drill_depth + 1;
    //         return {
    //             fuel: newFuel,
    //             drill_depth: newDrill_depth,
    //         }},
    //     loss: (state) => {
    //         const newFuel = state.fuel - 1;
    //         const newTemp = state.temperature + 1;
    //         return {
    //             fuel: newFuel,
    //             temperature: newTemp,
    //         }},
    // },
    {
        prompt: [
            'Problem: Drill is overheating!',
            'Action: Everyone blow into your Swissholes to lower drill temperature! (humidity sensor)'
        ],
        win: (state) => {
            const newFuel = state.fuel - 1;
            const newDrill_depth = state.drill_depth + 1;
            return {
                fuel: newFuel,
                drill_depth: newDrill_depth,
            }},
        loss: (state) => {
            const newFuel = state.fuel - 1;
            return {
                fuel: newFuel,
            }},
    },
    // {
    //     prompt: [
    //         'Problem: Blast and Comms have disconnected from the drill!',
    //         'Action:  Make like string cheese and stretch across from Blast to Comms now! (makey makey)'
    //     ],
    //     win: (state) => {
    //         const newTemp = state.temperature - 1;
    //         return {
    //             temperature: newTemp,
    //         }},
    //     loss: (state) => {
    //         const newTemp = state.temperature + 3;
    //         return {
    //             temperature: newTemp,
    //         }},
    // },
    {
        prompt: [
            'Problem: Drill is stuck in the rind, need to bleu a way through!',
            'Action: Comms shout “BE GOUDA” into your cheese phone! Navigation joystick pull up, throttle to full power! Weapons release C4 (button sequence)!'
        ],
        win: (state) => {
            const newFuel = state.fuel - 1;
            const newDrill_depth = state.drill_depth + 3;
            return {
                fuel: newFuel,
                drill_depth: newDrill_depth,
            }},
        loss: (state) => {
            const newFuel = state.fuel - 1;
            return {
                fuel: newFuel,
            }},
    },
    // {
    //     prompt: [
    //         'Problem: Drill is close to target, careful navigation required!',
    //         'Action: Comms adjust lactose levels 1 (knob)! Weapons set cheese dips to 1101101 (dip switch)! Navigation turn cheese wheel one circle counterclockwise!'
    //     ],
    //     win: (state) => {
    //         const newFuel = state.fuel - 1;
    //         const newDrill_depth = state.drill_depth + 1;
    //         return {
    //             fuel: newFuel,
    //             drill_depth: newDrill_depth,
    //         }},
    //     loss: (state) => {
    //         const newFuel = state.fuel - 1;
    //         return {
    //             fuel: newFuel,
    //         }},
    // },
    // {
    //     prompt: [
    //         'Problem: You’ve hit an unstable cheese cave that’s on the verge of collapse.',
    //         'Action: Ricotta hurry up and crawl to the station on your right',
    //         '[If they need to maintain their stations because of role assignments then we can immediately serve up another set of instructions like “This way looks precarious.  Back to the station on your left!” to get them back to their original station]'
    //     ],
    //     win: (state) => {
    //         return {
    //         }},
    //     loss: (state) => {
    //         const newFuel = state.fuel - 3;
    //         return {
    //             fuel: newFuel,
    //         }},
    // },
    {
        prompt: [
            'Problem: Drill needs to be stabilized.',
            'Action: All hands on the cheeseboard! Press down! (Hands on control panel/stations)',
        ],
        win: (state) => {
            const newFuel = state.fuel - 1;
            const newDrill_depth = state.drill_depth + 1;
            return {
                fuel: newFuel,
                drill_depth: newDrill_depth,
            }},
        loss: (state) => {
            const newDrill_depth = state.drill_depth - 2;
            return {
                drill_depth: newDrill_depth,
            }},
    },
    {
        prompt: [
            'Problem: We’ve hit hard cheese!',
            'Action: All hands on the cheeseboard! Press down! (Hands on control panel/stations)',
        ],
        win: (state) => {
            const newFuel = state.fuel - 2;
            const newDrill_depth = state.drill_depth + 1;
            return {
                fuel: newFuel,
                drill_depth: newDrill_depth,
            }},
        loss: (state) => {
            const newFuel = state.fuel - 2;
            const newDrill_depth = state.drill_depth;
            return {
                fuel: newFuel,
                drill_depth: newDrill_depth,
            }},
    },
    // {
    //     prompt: [
    //         'Problem: Drill needs synchronization!',
    //         'Action: Everybody needs to do the Macarena dance routine (while singing ‘Macaroni’ instead of macarena) synchronously',
    //     ],
    //     win: (state) => {
    //         const newFuel = state.fuel - 1;
    //         const newDrill_depth = state.drill_depth + 1;
    //         const newTemp = state.temperature + 1;
    //         return {
    //             fuel: newFuel,
    //             drill_depth: newDrill_depth,
    //             temperature: newTemp,
    //         }},
    //     loss: (state) => {
    //         const newFuel = state.fuel - 1;
    //         const newDrill_depth = state.drill_depth;
    //         return {
    //             fuel: newFuel,
    //             drill_depth: newDrill_depth,
    //         }},
    // },
    {
        prompt: [
            'Problem: Drill is leaking!',
            'Action: Plug the holes with your fingers! (ultrasonic sensor)',
        ],
        win: (state) => {
            const newFuel = state.fuel - 1;
            const newDrill_depth = state.drill_depth + 1;
            const newTemp = state.temperature;
            return {
                fuel: newFuel,
                drill_depth: newDrill_depth,
                temperature: newTemp,
            }},
        loss: (state) => {
            const newFuel = state.fuel - 3;
            const newDrill_depth = state.drill_depth;
            const newTemp = state.temperature;
            return {
                fuel: newFuel,
                drill_depth: newDrill_depth,
                temperature: newTemp,
            }},
    },
];

const onUpdateGameParams = (state) =>
{
    if (state.drill_depth >= state.cheese_depth)
    {
        state.game_state = GameState.won;
        return state;
    }

    if(state.temperature >= state.temperature_max)
    {
        state.game_state = GameState.lost;
        return state;
    }

    if(state.temperature >= state.temperature_max)
    {
        state.game_state = GameState.lost;
        return state;
    }

    if(state.fuel <= state.fuel_min)
    {
        state.game_state = GameState.lost;
    }

    return state;
};

const onUpdateTimerTick = (state) => {
    let new_time_left = state.time_left - 1;
    let result = {};
    if(new_time_left <= 0)
    {
        result = state.step().loss(state);
        state = resetTimeLeft(state);
        const step_index = { step_index:state.step_index + 1 % steps.length };
        return {
            ...state,
            ...result,
            ...step_index,
        };
    }
    return {
        ...state,
        ...result,
        ...{time_left: new_time_left}
    };

};

const resetTimeLeft = (state) => {
    return {
        ...state,
        ...{time_left: state.step_time}
    };
};

const gameReducer = (state=initialState, action) => {

    console.log(state.step_index);

    if(action.type === Actions.reset_game)
    {
        return {
            ...initialState
        }
    }

    if(action.type === Actions.start_game)
    {
        return {
            ...state,
            game_state: GameState.in_progress,
        }
    }

    if(action.type === "TIMER_SECOND_TICK")
    {
        state = onUpdateTimerTick(state);
        state = onUpdateGameParams(state);
        return state;
    }
    if(action.type === "STEP_WIN")
    {
        // var audio = new Audio('file://' + __dirname + '/assets/sounds/Ta Da-SoundBible.com-1884170640.wav');
        // audio.play();
        console.log(action);
        const result = state.step().win(state);
        state = resetTimeLeft(state);
        const step_index = { step_index:state.step_index + 1 % steps.length };
        state = {
            ...state,
            ...result,
            ...step_index,
        };
        state = onUpdateGameParams(state);
        return state;
    }
    if(action.type === "STEP_LOSS")
    {
        console.log(action);
        const result = state.step().loss(state);
        state = resetTimeLeft(state);
        const step_index = { step_index:state.step_index + 1 % steps.length };
        state = {
            ...state,
            ...result,
            ...step_index
        };
        state = onUpdateGameParams(state);
        return state;
    }
    return state;
};

export default gameReducer;