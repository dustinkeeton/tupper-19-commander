import React, { Component } from 'React';
import { connect } from 'react-redux';
import {GameState, Actions} from '../constants';
import GameOver from './game_over';
import GameInProgress from './game_in_progress'

class Game extends Component {

    render() {
        const {
            game_state
        } = this.props.game;

        if(game_state === GameState.idle)
        {
            return (
                <h1 onClick={this.props.onStartGame}>Press the bright greeen button!</h1>
            );
        }

        if(game_state === GameState.lost ||
           game_state === GameState.won)
        {
            return (
              <GameOver />
            );
        }

        return (
            <GameInProgress />
        );
    }

}

const mapStateTpProps = (state) => {
    return {
        game: state.GameReducer
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onStartGame: () => dispatch({type: Actions.start_game}),
    };
};

export default connect(mapStateTpProps, mapDispatchToProps)(Game);