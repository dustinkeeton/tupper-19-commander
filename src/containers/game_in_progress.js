import React, { Component } from 'React';
import ReactDOM from 'react-dom'
import { connect } from 'react-redux';
import { Gauge } from '../assets/gague';
import Flexbox from 'flexbox-react';
import {GameState} from '../constants';
import { steps } from '../store/reducer_game';

import { ipcRenderer } from 'electron';


class GaugeWrapper extends Component {
    componentDidMount() {
        var target = ReactDOM.findDOMNode(this);
        this.gauge = new Gauge(target).setOptions(this.props.options);
        this.gauge.maxValue = this.props.max;
        this.gauge.set(this.props.value);
    }
    render() {
        if(this.gauge) {
            this.gauge.set(this.props.value);
        }
        return <canvas width={this.props.width} height={this.props.height}/>
    }
};

var opts_pressure_temp = {
    angle: 0.15, // The span of the gauge arc
    lineWidth: 0.33, // The line thickness
    radiusScale: 1, // Relative radius
    pointer: {
        length: 0.68, // // Relative to gauge radius
        strokeWidth: 0.055, // The thickness
        color: '#008000' // Fill color
    },
    limitMax: true,     // If false, max value increases automatically if value > maxValue
    limitMin: true,     // If true, the min value of the gauge will be fixed
    colorStart: '#CF1111',   // Colors
    colorStop: '#810DDA',    // just experiment with them
    strokeColor: '#E0E0E0',  // to see which ones work best for you
    generateGradient: true,
    highDpiSupport: true,     // High resolution support
    percentColors: [[0.0, "#008000" ], [0.50, "#f9c802"], [1.0, "#ff0000"]]
};

var opts_fuel = {
    angle: 0.15, // The span of the gauge arc
    lineWidth: 0.33, // The line thickness
    radiusScale: 1, // Relative radius
    pointer: {
        length: 0.68, // // Relative to gauge radius
        strokeWidth: 0.055, // The thickness
        color: '#008000' // Fill color
    },
    limitMax: true,     // If false, max value increases automatically if value > maxValue
    limitMin: true,     // If true, the min value of the gauge will be fixed
    colorStart: '#CF1111',   // Colors
    colorStop: '#810DDA',    // just experiment with them
    strokeColor: '#E0E0E0',  // to see which ones work best for you
    generateGradient: true,
    highDpiSupport: true,     // High resolution support
    percentColors: [[0.0, "#ff0000" ], [0.50, "#f9c802"], [1.0, "#008000"]]
};
var opts_drill_depth = {
    angle: 0.15, // The span of the gauge arc
    lineWidth: 0.33, // The line thickness
    radiusScale: 1, // Relative radius
    pointer: {
        length: 0.68, // // Relative to gauge radius
        strokeWidth: 0.055, // The thickness
        color: '#008000' // Fill color
    },
    limitMax: true,     // If false, max value increases automatically if value > maxValue
    limitMin: true,     // If true, the min value of the gauge will be fixed
    colorStart: '#CF1111',   // Colors
    colorStop: '#810DDA',    // just experiment with them
    strokeColor: '#E0E0E0',  // to see which ones work best for you
    generateGradient: true,
    highDpiSupport: true,     // High resolution support
    percentColors: [[0.0, "#ff0000" ], [1.0, "#008000"]]
};

class GameInProgress extends Component {

    componentDidMount()
    {
        this.timeout = setInterval(this.props.onSecondTick, 1000);
    }

    componentWillReceiveProps(nextProps) {
        clearInterval(this.timeout);
        this.timeout = setInterval(this.props.onSecondTick, 1000);
        //this.props.tryPattern(0, 0);
    }

    componentWillUnmount() {
        clearInterval(this.timeout);
    }

    renderPrompts(prompt_list)
    {
        const list_items = prompt_list.map((item, index) =>
            <h1 key={index}>{item}</h1>
        );
        return (
            <Flexbox height="900px" flexDirection='row' alignItems='center' alignContent="space-between" flexWrap='wrap'>
                {list_items}
            </Flexbox>);
    }


    render() {
        const {
            fuel, temperature, pressure, time_left, drill_depth, pressure_max,
            cheese_depth, temperature_max
        } = this.props.game;

        const step = this.props.game.step();

        if(this.props.game.game_state === GameState.Idle)
        {
            return (
                <h1>Press the bright greeen button!</h1>
            );
        }

        return (
            <Flexbox flexDirection='row' alignContent='center'>
                <Flexbox flexDirection='column' alignItems='center'>
                    <h1 onClick={this.props.onStepWin}>Timer: {time_left} seconds!</h1>
                    <h1>Pressure: {pressure}</h1>
                    <GaugeWrapper width="500" options={opts_pressure_temp} max={pressure_max} value={pressure}/>
                    <h1 onClick={this.props.onStepLoss}>Fuel: {fuel}</h1>
                    <GaugeWrapper width="500" options={opts_fuel} max="10" value={fuel}/>
                    <h1>Temperature: {temperature}</h1>
                    <GaugeWrapper width="500" options={opts_pressure_temp} max={temperature_max} value={temperature}/>
                    <h1>Drill Depth: {drill_depth}</h1>
                    <GaugeWrapper width="500" options={opts_drill_depth} max={cheese_depth} value={drill_depth}/>
                </Flexbox>
                <div>
                    {this.renderPrompts(step.prompt)}
                    <h1 onClick={this.props.onStepWin}>Simulate Win</h1>
                </div>
            </Flexbox>
        );
    }

}

const mapStateTpProps = (state) => {
    return {
        game: state.GameReducer
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSecondTick: () => dispatch({type: "TIMER_SECOND_TICK"}),
        onStepWin: () => dispatch({type: 'STEP_WIN'}),
        onStepLoss: () => dispatch({type: 'STEP_LOSS'}),
        tryPattern: (console, pattern) => dispatch(
            {
                type: 'TRY_PATTERN',
                data: {
                    console: console,
                    pattern: pattern
                }
        })
    };
};

export default connect(mapStateTpProps, mapDispatchToProps)(GameInProgress);