import React, { Component } from 'React';
import ReactDOM from 'react-dom'
import { connect } from 'react-redux';
import { Gauge } from '../assets/gague';
import Flexbox from 'flexbox-react';
import {GameState, Actions} from '../constants';

import { ipcRenderer } from 'electron';

class GameOver extends Component {

    componentDidMount() {
        this.timeout = setTimeout(this.props.onGameOver, 5000);
    }

    componentWillUnmount() {
        clearTimeout(this.timeout);
    }


    render() {
        const {
            game_state
        } = this.props.game;

        return (
            <Flexbox flexDirection='row' alignContent='center'>
                <h1>Game Over: {game_state}</h1>
            </Flexbox>
        );
    }

}

const mapStateTpProps = (state) => {
    return {
        game: state.GameReducer
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onGameOver: () => dispatch({type: Actions.reset_game}),
    };
};

export default connect(mapStateTpProps, mapDispatchToProps)(GameOver);